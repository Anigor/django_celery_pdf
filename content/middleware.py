import json
from django.http import HttpResponse


class HandlingErrors:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request, *args, **kwargs):
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        return HttpResponse(json.dumps({'error': str(exception)}), content_type="application/json")
