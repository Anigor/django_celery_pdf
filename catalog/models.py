from django.db import models
from solo.models import SingletonModel


class Product(models.Model):

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Список товара"

    name = models.CharField("Название товара", max_length=250, default="")
    count = models.IntegerField("Количество на складе", default=0)

    def __str__(self):
        return f"{self.id} - {self.name}"


class Report(models.Model):

    class Meta:
        verbose_name = "Отчет"
        verbose_name_plural = "Список отчетов"

    name = models.CharField("Название отчета", max_length=250, default="")
    file_name = models.CharField("Название файла", max_length=250, default="")
    status = models.IntegerField("Статус", default=0)

    def get_status(self):
        if self.status == 0:
            return "Заявка на создание"
        if self.status == 1:
            return "Отчет создан"
        if self.status == 2:
            return "Отчет отправлен"
        if self.status == 3:
            return "Ошибка отправки отчета"

    def __str__(self):
        return f"{self.id} - {self.get_status()}"


class SiteConfiguration(SingletonModel):
    """Настройки сайта"""

    class Meta:
        verbose_name = "Настройкий сайта"
        verbose_name_plural = "Настройкий сайта"

    email = models.CharField(max_length=255, default='Email')

    def __str__(self):
        return "Настройкий сайта"
