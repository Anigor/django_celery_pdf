import json
from django.test import TestCase
from catalog.models import *
from catalog.serializer import ProductSerializer
from catalog.service import product_update_count


class TestUpdateProduct(TestCase):

    def setUp(self):
        product = Product(name="Табурет", count=5)
        product.save()

    def test_update_product(self):
        products = Product.objects.all()
        self.assertEqual(len(products), 1)
        product = products[0]
        product.count = 10
        serializer = ProductSerializer(product)
        self.assertEqual(serializer.data, {'id': 1, 'name': 'Табурет', 'count': 10})

    def test_product_update_count(self):
        products = Product.objects.all()
        self.assertEqual(len(products), 1)
        product = products[0]
        try:
            product_update_count(None, None)
        except Exception as exc:
            self.assertEqual(str(exc), "Не найден обязательный параметр")

        try:
            product_update_count(0, 5)
        except Exception as exc:
            self.assertEqual(str(exc), "Товар не найден")

        serializer_data = product_update_count(product.id, product.count)
        serializer = ProductSerializer(product)
        self.assertEqual(serializer_data, serializer.data)
