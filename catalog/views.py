from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from catalog.models import Product
from catalog.serializer import ProductSerializer, ProductCountSerializer
from catalog.service import product_update_count, create_task_convert_pdf


class ProductView(mixins.CreateModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):

    serializers = {
        'default': ProductSerializer,
        'list': ProductSerializer,
    }

    def get_serializer_class(self):
        return self.serializers.get(self.action, self.serializers['default'])

    def get_queryset(self):
        return Product.objects.get_queryset()

    def create(self, request, *args, **kwargs):
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        raise Exception(serializer.error_messages)

    def update(self, request, *args, **kwargs):
        if kwargs.get('pk') is not None:
            data = request.data
            products = Product.objects.filter(id=kwargs.get('pk'))
            if len(products):
                serializer = ProductSerializer(products[0], data=data, many=False)
                if serializer.is_valid():
                    serializer.save()
                    create_task_convert_pdf()
                    return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    raise Exception(serializer.error_messages)
            else:
                raise Exception("Товар не найден")
        else:
            raise Exception("Не найден обязательный параметр")

    @swagger_auto_schema(methods=['put', 'post'], request_body=ProductCountSerializer)
    @action(methods=['put', 'post'], detail=True, description="Изменение количества")
    def update_count(self, request, pk=None):
        return Response(product_update_count(pk, request.data.get('count')))
