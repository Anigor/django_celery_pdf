import time
from celery import shared_task
from fpdf import FPDF

from catalog.models import Product, Report, SiteConfiguration
from catalog.sendmail import send_email
from content.settings import STATIC_ROOT


class PDF(FPDF):
    """Класс для формирования таблицы в pdf"""

    def basic_table(self, headings, rows):
        for heading in headings:
            self.cell(40, 7, heading, 1)
        self.ln()
        for row in rows:
            for col in row:
                self.cell(40, 6, col, 1)
            self.ln()

    def improved_table(self, headings, rows, col_widths=(50, 50)):
        for col_width, heading in zip(col_widths, headings):
            self.cell(col_width, 7, heading, 1, 0, "C")
        self.ln()
        for row in rows:
            self.cell(col_widths[0], 6, row[0], "LR", 0, "R")
            self.cell(col_widths[1], 6, row[1], "LR", 0, "R")
            self.ln()
        # Линия закрытия
        self.cell(sum(col_widths), 0, "", "T")

    def colored_table(self, headings, rows, col_widths=(50, 50)):
        self.set_fill_color(255, 100, 0)
        # цвет текста
        self.set_text_color(255)
        # цвет линий таблицы
        self.set_draw_color(255, 0, 0)
        # ширина линии
        self.set_line_width(0.3)
        # жирный шрифт
        self.set_font(style="B")
        for col_width, heading in zip(col_widths, headings):
            self.cell(col_width, 7, heading, 1, 0, "C", True)
        self.ln()
        # Восстановление цвета и шрифта:
        self.set_fill_color(224, 235, 255)
        self.set_text_color(0)
        self.set_font()
        fill = False
        for row in rows:
            self.cell(col_widths[0], 6, row[0], "LR", 0, "L", fill)
            self.cell(col_widths[1], 6, row[1], "LR", 0, "R", fill)
            self.ln()
            fill = not fill
        self.cell(sum(col_widths), 0, "", "T")


@shared_task
def create_pdf(id=0):
    """Формируем pdf файл со списком содержимого товара"""

    products = Product.objects.all()
    reports = Report.objects.filter(id=id).all()

    if len(reports) == 1:
        report = reports[0]
        pdf = PDF()
        # включаем TTF шрифты, поддерживающие кириллицу
        pdf.add_font("Sans", style="", fname=f"{STATIC_ROOT}/font/helvetica_cyr_boldoblique.ttf", uni=True)
        pdf.set_font("Sans", size=14)
        pdf.add_page()
        pdf.improved_table(["Товар", "Количество"], [[tovar.name, str(tovar.count)] for tovar in products])

        report.file_name = f"tmp_{time.time()}.pdf"
        pdf.output(f"{STATIC_ROOT}/pdf/{report.name}")
        report.status = 1
        report.save()

        # Отправляем отчет
        try:
            config = SiteConfiguration.objects.get()
            send_email(config.email, f"Новый отчет №{report.id}", "Сформирован ноывй отчет", [f"{STATIC_ROOT}/pdf/{report.name}"])
            report.status = 2
            report.save()
        except Exception as exp:
            raise Exception(str(exp))
            report.status = 3
            report.save()
