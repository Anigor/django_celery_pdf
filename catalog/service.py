import time
from catalog.models import Product, Report
from catalog.serializer import ProductSerializer
from catalog.tasks import create_pdf


def create_task_convert_pdf():
    """Создаю задачу в selery по генерации отчета"""

    report = Report.objects.create(name=f"Отчет №{time.time()}")
    report.save()
    create_pdf.delay(report.id)


def product_update_count(pk=None, count=None):
    """Изменение цены товара"""

    if pk is not None and count is not None:
        products = Product.objects.filter(id=pk).all()
        if len(products) == 1:
            product = products[0]
            count_old = product.count
            product.count = count
            product.save()
            if count_old != count:
                create_task_convert_pdf()
            serializer = ProductSerializer(product)
            return serializer.data
        else:
            raise Exception("Товар не найден")
    raise Exception("Не найден обязательный параметр")
