from django.contrib import admin
from .models import Product, Report, SiteConfiguration


admin.site.register(Product)


@admin.register(Report)
class VideoAdmin(admin.ModelAdmin):
    model = Report
    list_display = ('id', 'name', 'get_status', 'file_name',)
    readonly_fields = ('get_status',)


@admin.register(SiteConfiguration)
class SiteConfigurationAdmin(admin.ModelAdmin):
    list_display = ("email",)
