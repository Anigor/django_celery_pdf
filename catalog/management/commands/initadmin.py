from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            admin = User.objects.create_superuser(email="test@tets.ru", username="admin", password="1qaz")
            admin.is_active = True
            admin.is_admin = True
            admin.save()

        else:
            print('Admin accounts can only be initialized if no Accounts exist')