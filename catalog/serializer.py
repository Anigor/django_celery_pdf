from rest_framework import serializers
from .models import Product


class ProductSerializer(serializers.ModelSerializer):
    """Товар"""

    class Meta:
        model = Product
        fields = '__all__'


class ProductCountSerializer(serializers.ModelSerializer):
    """Товар"""

    class Meta:
        model = Product
        fields = ['count']
