FROM python:3.9-buster

WORKDIR /usr/src/app
COPY . .
ENV TZ=Asia/Yekaterinburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN pip install -r req.txt